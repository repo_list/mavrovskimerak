<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mavrovski_merak_test' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'O]1xG<8evr3;PnK[5Q,tTPHtwfe0fQNE[sR;f>#b6Acm15Dd%A{@,ehnw%=-vP+ ' );
define( 'SECURE_AUTH_KEY',  '=*qm-R$7zk/!^fZ#wIe{ >ANo%k7|ci1p]BS+:l(mHuOcB$|H}dVw`}/Yu#0;we;' );
define( 'LOGGED_IN_KEY',    '<t3cYlNO9lxx/SBo[6DEJ~|uV8{*4$x,,mC.;Ls&D/mY>#x.4UulmwFFOcoH9j Y' );
define( 'NONCE_KEY',        '{9IUvrz?B)FfrNJO&S^n[A^i6/]4@7_UBtoi-68p-vm=MPUiJpF9`2gKi8r79DMl' );
define( 'AUTH_SALT',        'r{9Iy?_U3208V`uGL(/u6DM%.N6+2=/%`]$Irh=#h9E8C]&:}e |,VNa;J@>$S]L' );
define( 'SECURE_AUTH_SALT', '-oKNyS#pP;|>[&Ly42P51rS(G!U#Z5NBS.hBWeE7dUVT.&VF88Y6nyl^i`@%=T0Z' );
define( 'LOGGED_IN_SALT',   'DYb?<@GkM>_ksKE^uXPC6tW1dcbI,<Q]iij=pZF+:RY+,ZwtA17z/!Lr+3GbHYmz' );
define( 'NONCE_SALT',       's+:O!Y1zbj%`zZLParRlM5mGv{^ZiV*T`e|4H=5o:_(Q!T@Gp-o<OAf9cM8-),}z' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
